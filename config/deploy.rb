set :application, "datagen"

require 'capistrano_colors'

require "bundler/capistrano"
set :bundle_flags, "--no-deployment --without test development"

require "rvm/capistrano"
set :rvm_ruby_string, '2.0.0'
set :rvm_type, :user

set :rvm_path, '/home/railsdev3/.rvm'
set :rvm_bin_path, "#{rvm_path}/bin"
set :rvm_lib_path, "#{rvm_path}/lib"

#ssh_options[:password] = '2oo8ii22mrg'
set :repository,  'https://ssk526@bitbucket.org/ssk526/datagen.git'
#set :repository,  'git@bitbucket.org:ssk526/datagen.git'
set :scm, :git
set :branch, "master"
set :deploy_via, :remote_cache
set :deploy_to, "/var/www/rails3/datagen"
set :rails_env, "production"

set :server_ip, '219.94.251.107'

server "#{server_ip}:22", :app, :web, :db, :primary => true

# 接続ユーザ
set :user, 'railsdev3'
set :user_group, 'rails'

# sshオプションン
ssh_options[:keys] = %w(/home/railsdev3/.ssh/id_rsa)
ssh_options[:auth_methods] = %w(publickey)

# sudoパスワード
set :password, "2oo8ii22mrg"
set :use_sudo, true

ssh_options[:forward_agent] = true
default_run_options[:pty] = true

set :normalize_asset_timestamps, false
set :keep_releases, 5

namespace :assets do
  task :precompile, :roles => :web do
    run "cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec rake assets:precompile"
  end
end


# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

=begin
role :web, "#{server_ip}"                          # Your HTTP server, Apache/etc
role :app, "#{server_ip}"                          # This may be the same as your `Web` server
role :db,  "#{server_ip}", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"
=end

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
 namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :web, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#     run "#{sudo :as => 'nginx'} /usr/local/rvm/nginx/rails32/sbin/nginx -s stop"
#     run "#{sudo :as => 'nginx'} /usr/local/rvm/nginx/rails32/sbin/nginx"
     run "/usr/local/rvm/nginx/rails32/sbin/nginx -s stop"
     run "/usr/local/rvm/nginx/rails32/sbin/nginx"
   end
 end

#after :deploy, "maintenance:on"
after :deploy, "deploy:migrate"
after :deploy, "assets:precompile"
after :deploy, "deploy:restart"
#after :deploy, "maintenance:off"
after :deploy, "deploy:cleanup"
