# -*- coding: utf-8 -*-
class MyLib::MyConfList < Array

  attr_reader :configures
  attr_writer :concat_list

  @@required_params = {
      'data_format' => {'for_user' => 'パラメータが不正です。', 'for_system' => 'パラメータdata_formatが存在しません。'},
      'rownum' => {'for_user' => 'パラメータが不正です。', 'for_system' => 'パラメータrownumが存在しません。'}
  }

  def initialize
    @configures = Hash.new
    @configures[:data_format] = 0
    @configures[:rownum] = 0
    @configures[:table] = Hash.new
    @configures[:csv] = Hash.new
    @configures[:sql] = Hash.new

    # 出力形式に応じたオプション
    # 形式やオプションを増やした場合はここで追加。
    # tableオプションン
    @configures[:table][:header_st] = {'field_name' => false, 'column_name' => false, 'datatype' => false }
    # csvオプションン
    @configures[:csv][:header_st] = {'field_name' => false, 'column_name' => false, 'datatype' => false }
    # sqlオプションン
    @configures[:sql][:table_name] = ''
    @configures[:sql][:column_qt] = "1"

    @concat_list = nil
  end

  # my_conf_itemクラスのget_valueメソッドによって1カラムずつデータを取得する。
  def get_row
    row_hash = Array.new
    self.each{|item|
      # 2011-11-19 仕様変更によりget_valueは1つ以上のハッシュを含んだ配列を返す。
      # [{:seq => 'item01', :value => 'val1', :options =>{..}}, {:seq => ...} ]
      row_hash.concat(item.get_value)
    }

    # 連結アイテムによる値を取得。
    row_hash.concat(@concat_list.get_row(row_hash))

    return row_hash
  end

  def get_rows(rownum)
    num = rownum > 0 ? rownum : 0
    my_rows = MyLib::MyRows.new
    1.upto(num){|i|
      # 設定情報に従って1レコードを出力
      my_rows.add_row(get_row)
    }
    my_rows.field_names = get_field_names
    my_rows.column_names = get_column_names
    my_rows.configures = @configures
    return my_rows
  end

  def get_rows_all
    return get_rows(@configures[:rownum])
  end

  # フィールド名を各アイテムから再帰的に取得
  def get_field_names
    res = Array.new
    self.each{|item|
      # get_field_name は子アイテムを再帰的に呼び出し、[{:seq => 'item01', :name => 'colmun1'}, .. ]
      # を取得する。
      res.concat(item.get_field_name)
    }

    # 連結アイテムによる値を取得。
    res.concat(@concat_list.get_field_names)

    return res
  end

  # カラム名を各アイテムから再帰的に取得
  def get_column_names
    res = Array.new
    self.each{|item|
      # get_column_name は子アイテムを再帰的に呼び出し、[{:seq => 'item01', :name => 'colmun1'}, .. ]
      # を取得する。
      res.concat(item.get_column_name)
    }

    # 連結アイテムによる値を取得。
    res.concat(@concat_list.get_column_names)

    return res
  end

  # 全体設定を設定する。
  # 全体設定の項目を変更したときにはここを修正する。
  # 必須項目に対するvalidationを追加したいときは@@required_paramsに追加
  def set_gen_confs(params)

    to_validate(params)

    @configures[:data_format] = params['data_format']
    @configures[:rownum] = params['rownum'].to_i
    [:table, :csv].each{|format|
      if params[format].keys.include?('header_st')
        params[format]['header_st'].each{|key, value|
          if @configures[format][:header_st].keys.include?(key) && value=="1"
            @configures[format][:header_st][key] = true
          end
        }
      end
    }
    if params['sql']['table_name']
      @configures[:sql][:table_name] = params['sql']['table_name']
    end
    unless params['sql']['column_qt'].blank?
      @configures[:sql][:column_qt] = params['sql']['column_qt']
    end
  end

  # 与えられたアイテムが従属アイテムなら、すでに格納されたアイテムを走査する。
  # そうでなければpushする。
  def set_item(item)
    if item.depend_on?
      self.each{|ci|
        if ci.set_child_item(item)
          return true
        end
      }
      return false
    else
      self.push(item)
      return true
    end
  end

  # 必須のパラメータを@@required_paramsに追加すると検証できる。
  def to_validate(params)

    @@required_params.keys.each{|key|
      unless params.keys.include?(key)
        ex = MyLib::MyParamErrorException.new(@@required_params[key]['for_system'])
        ex.user_message = @@required_params[key]['for_user']
        raise ex
      end
    }

    if params['rownum'].to_i > 50000
      ex = MyLib::MyParamErrorException.new("パラメータが不正です。（rownum>#{params['rownum']}）")
      ex.user_message = "行数は50000以下にしてください。"
      raise ex
    end

  end

end
