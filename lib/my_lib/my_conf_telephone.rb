# -*- coding: utf-8 -*-
class MyLib::MyConfTelephone < MyLib::MyConfItem

  @@delimitters = {'tel_delimitter1' => '', 'tel_delimitter2' => '-'}

  @@tel_pattern = {
    'tel01' => ['tel01', 'tel02', 'tel03', 'tel04'],
    'tel05' => ['tel05'],
    'tel06' => ['tel06'],
    'tel07' => ['tel07'],
    'tel08' => ['tel08'],
    'tel09' => ['tel09', 'tel10'],
    'tel11' => ['tel11', 'tel12'],
    'tel13' => ['tel13'],
    'tel14' => ['tel14']
  }

  @@tel_format = {
    'tel01'=>{'pattern_no' => 1,'type_no' => 1,'format' => {'prefix' => '', 'lv1' => 1, 'lv2' => 4, 'lv3' => 4}},
    'tel02'=>{'pattern_no' => 2,'type_no' => 1,'format' => {'prefix' => '', 'lv1' => 2, 'lv2' => 3, 'lv3' => 4}},
    'tel03'=>{'pattern_no' => 3,'type_no' => 1,'format' => {'prefix' => '', 'lv1' => 3, 'lv2' => 2, 'lv3' => 4}},
    'tel04'=>{'pattern_no' => 4,'type_no' => 1,'format' => {'prefix' => '', 'lv1' => 4, 'lv2' => 1, 'lv3' => 4}},
    'tel05'=>{'pattern_no' => 5,'type_no' => 2,'format' => {'prefix' => '20', 'lv1' => 0, 'lv2' => 4, 'lv3' => 4}},
    'tel06'=>{'pattern_no' => 6,'type_no' => 3,'format' => {'prefix' => '50', 'lv1' => 0, 'lv2' => 4, 'lv3' => 4}},
    'tel07'=>{'pattern_no' => 7,'type_no' => 4,'format' => {'prefix' => '60', 'lv1' => 0, 'lv2' => 4, 'lv3' => 4}},
    'tel08'=>{'pattern_no' => 8,'type_no' => 5,'format' => {'prefix' => '70', 'lv1' => 0, 'lv2' => 4, 'lv3' => 4}},
    'tel09'=>{'pattern_no' => 9,'type_no' => 6,'format' => {'prefix' => '80', 'lv1' => 0, 'lv2' => 4, 'lv3' => 4}},
    'tel10'=>{'pattern_no' =>10,'type_no' => 6,'format' => {'prefix' => '90', 'lv1' => 0, 'lv2' => 4, 'lv3' => 4}},
    'tel11'=>{'pattern_no' =>11,'type_no' => 7,'format' => {'prefix' => '120', 'lv1' => 0, 'lv2' => 3, 'lv3' => 3}},
    'tel12'=>{'pattern_no' =>12,'type_no' => 7,'format' => {'prefix' => '800', 'lv1' => 0, 'lv2' => 3, 'lv3' => 4}},
    'tel13'=>{'pattern_no' =>13,'type_no' => 8,'format' => {'prefix' => '570', 'lv1' => 0, 'lv2' => 3, 'lv3' => 3}},
    'tel14'=>{'pattern_no' =>14,'type_no' => 9,'format' => {'prefix' => '990', 'lv1' => 0, 'lv2' => 3, 'lv3' => 3}}
  }

  def initialize(conf)
    super
    @tel_type = {
      'tel01' => conf['tel01'],
      'tel05' => conf['tel05'],
      'tel06' => conf['tel06'],
      'tel07' => conf['tel07'],
      'tel08' => conf['tel08'],
      'tel09' => conf['tel09'],
      'tel11' => conf['tel11'],
      'tel13' => conf['tel13'],
      'tel14' => conf['tel14']
    }
    @tel_delimitter = {'tel_delimitter1' => conf['tel_delimitter1'], 'tel_delimitter2' => conf['tel_delimitter2']}
  end

  def get_value

    _param = get_checked_keys(@tel_type)
    _dels  = get_checked_keys(@tel_delimitter)

    if depend_on?
    else
      _tel_pattern = @@tel_pattern[_param[rand(_param.size)]]
      _tel_pattern_no = _tel_pattern[rand(_tel_pattern.size)]
      _tel_format = @@tel_format[_tel_pattern_no]
      num = get_telephone_number(_tel_format)
      @data = num
    end

	res = get_formatted_number(data, @@delimitters[_dels[rand(_dels.size)]])

    _res_array  = return_value(res)
    _res_array.concat(get_child_values)

    return _res_array
  end

  def get_telephone_number(frm)

    fm = frm['format'] 

    num = ['', '', '']
    if fm['prefix'] == ''
      # 固定電話の場合、ゼロを含めない。（その他の番号とかぶるため）
      fm['lv1'].times{|i|
        num[0] += sprintf("%d", rand(9)+1)
      }
    else
      num[0] = fm['prefix']
    end

    fm['lv2'].times{|i|
      num[1] += sprintf("%d", rand(10))
    }

    fm['lv3'].times{|i|
      num[2] += sprintf("%d", rand(10))
    }

    return num

  end

  def get_formatted_number(tel, delimitter)
    return '0' + tel.join(delimitter)
  end

end
