# -*- coding: utf-8 -*-
class MyLib::MyConfSex < MyLib::MyConfGivenName

  @@sex_status = {'1' => '男', '2' => '女'}

  def initialize(conf)
    super
    @label = {'1' => conf['sex_label1'], '2' => conf['sex_label2'], '3' => conf['sex_label3']}
  end

  def get_value

    if depend_on?
      case data_status[:data_name]
        when 'given_name'
          st = data.sex
        when 'full_name'
          st = data['gn'].sex
        when 'sex'
          st = data
        else
          raise "data_status[:data_name]（#{data_status[:data_name]}）が不正です。"
      end
    else
      _arr = Array.new
      @label.each{|key, val|
        unless val.blank?
          _arr.push(key)
        end
      }
      st = sprintf("%d", _arr[rand(_arr.size)])
      set_data(st)
    end

    res = @label[st]

    _res_array  = return_value(res)
    _res_array.concat(get_child_values)

    return _res_array
  end

end
