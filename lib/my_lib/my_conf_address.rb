# -*- coding: utf-8 -*-
class MyLib::MyConfAddress < MyLib::MyConfItem

  @@name_type = {"1" => :name, "2" => :pronunciation}

  def initialize(conf)
    super
    @zenhan    = {'zenhan1' => conf['zenhan1'], 'zenhan2' => conf['zenhan2'], 'zenhan3' => conf['zenhan3'], 'zenhan4' => conf['zenhan4']}
    @uplow    = {'uplow1' => conf['uplow1'], 'uplow2' => conf['uplow2'], 'uplow3' => conf['uplow3']}
    if @@name_type.include?(conf['type'])
      @kanji_kana_st = conf['type']
    else
      @kanji_kana_st = "1"
    end
  end

  def conv_zenhan(str)
    if @kanji_kana_st == "1"
      return str
    end
    _param = get_checked_keys(@zenhan)
    res = ''
    case _param[rand(_param.size)]
      when 'zenhan1'
        res = str
      when 'zenhan2'
        res = hira2kata(str)
      when 'zenhan3'
        res = zen2han(hira2kata(str))
      when 'zenhan4'
        _uplows = get_checked_keys(@uplow)
        res = RomanKana.kanaroman(str).tr("０-９－","0-9-")
        
        case _uplows[rand(_uplows.size)]
          when 'uplow1'
          when 'uplow2'
            res_tmp = res[0, 1].upcase
            if res.size > 1
              res_tmp += res[1, (res.size-1)]
            end
            res = res_tmp
          when 'uplow3'
            res = res.upcase
        end
    end
    return res
  end
end
