# -*- coding: utf-8 -*-
class MyLib::MyParamErrorException < Exception

  attr_writer :user_message

  def initialize(error_message = nil)
    super
    @user_message = ''
  end

  def user_message
    if @user_message.blank?
      return 'エラーが発生しました。'
    else
      return @user_message
    end
  end

end
