# -*- coding: utf-8 -*-
class MyLib::MyConfParser

  @@required_params = {
      'genconf' => {'for_user' => 'パラメータが不正です。', 'for_system' => 'パラメータgenconfが存在しません。'},
      'conf' => {'for_user' => 'パラメータが不正です。', 'for_system' => 'パラメータconfが存在しません。'}
  }

  attr_reader :conf_list

  def initialize(params)
    @conf_list = nil
    @concat_list = nil
    get_confs(params)
  end

  # paramsを丸ごと渡す
  def get_confs(params)

    to_validate(params)

    list_concat_tmp = Array.new
    list_tmp = Array.new
    my_conf_list = MyLib::MyConfList.new
    my_conf_list.set_gen_confs(params['genconf'])
    my_concat_list = MyLib::MyConfListConcat.new

    all_params = params['conf'].to_a.sort{|a, b|
      a[1]['item_seq'] <=> b[1]['item_seq']
    }

    # 全アイテム分のparamを各アイテムごとに分割。
    all_params.each{|key, val|
      if val['item_type'] == 'family_name'
        conf = MyLib::MyConfFamilyName.new(val)
      elsif val['item_type'] == 'given_name'
        conf = MyLib::MyConfGivenName.new(val)
      elsif val['item_type'] == 'full_name'
        conf = MyLib::MyConfFullName.new(val)
      elsif val['item_type'] == 'age'
        conf = MyLib::MyConfAge.new(val)
      elsif val['item_type'] == 'number'
        conf = MyLib::MyConfNumber.new(val)
      elsif val['item_type'] == 'prefecture'
        conf = MyLib::MyConfPrefecture.new(val)
      elsif val['item_type'] == 'telephone'
        conf = MyLib::MyConfTelephone.new(val)
      elsif val['item_type'] == 'zip_code'
        conf = MyLib::MyConfZipCode.new(val)
      elsif val['item_type'] == 'address'
        conf = MyLib::MyConfFullAddress.new(val)
      elsif val['item_type'] == 'datetime'
        conf = MyLib::MyConfDateTime.new(val)
      elsif val['item_type'] == 'sex'
        conf = MyLib::MyConfSex.new(val)
      elsif val['item_type'] == 'user_list'
        conf = MyLib::MyConfUserList.new(val)
      elsif val['item_type'] == 'concat'
        conf = MyLib::MyConfConcat.new(val)
	  else
        conf = MyLib::MyConfItem.new(val)
      end

      case val['item_type']
        when 'concat'
          list_concat_tmp.push(conf)
        else
          list_tmp.push(conf)
      end
    } 

    # 連結用リストに各アイテムの情報をセット。
    _conf_array = Array.new
    _conf_array.concat(list_tmp)
    _conf_array.concat(list_concat_tmp)
    list_concat_tmp.each{|item|
      _item1 = nil
      _item1 = get_item_by_id(_conf_array, item.item1_id)
      if _item1
        item.set_item1(_item1)
      else
        raise "Item not found! #{item.item_name} #{item.item1_id}"
      end

      _item2 = nil
      _item2 = get_item_by_id(_conf_array, item.item2_id)
      if _item2
        item.set_item2(_item2)
      else
        raise "Item not found! #{item.item_name} #{item.item2_id}"
      end
    }

    # 各アイテムの従属関係を元にツリー状にする。
    item_tmp = list_tmp.pop
    while item_tmp do
      fg = false

      # 未処理アイテムを走査し、親アイテムがあればそのアイテムの子にする。
      if item_tmp.depend_on?
        list_tmp.each{|item|
          if item.set_child_item(item_tmp)
            fg = true
            break
          end
        }
      end

      # 非従属アイテム、または未処理アイテム内に親アイテムが無い場合、リストにセットする。
      unless fg
        unless my_conf_list.set_item(item_tmp)
          raise 'Bad depend:' + item_tmp.item_id
        end
      end

      item_tmp = list_tmp.pop
    end
    @conf_list = my_conf_list

    list_concat_tmp.each{|item|
      my_concat_list.set_item(item)
    }
    @concat_list = my_concat_list

    @conf_list.concat_list = my_concat_list

    return @conf_list

  end

  def to_validate(params)

    ex = MyLib::MyParamErrorException.new
    @@required_params.keys.each{|key|
      unless params.keys.include?(key)
        ex.messages = @@required_params[key]['for_system']
        ex.user_message = @@required_params[key]['for_user']
        raise ex
      end
    }
    
  end

  def get_item_by_id(list, item_id)
    _res = nil
    list.each{|item|
      if item.item_id == item_id
        _res = item
      end
    }
    return _res
  end
end
