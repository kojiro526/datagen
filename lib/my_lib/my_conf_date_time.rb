# -*- coding: utf-8 -*-
class MyLib::MyConfDateTime < MyLib::MyConfItem

  def initialize(conf)
    super
    @min_date = Date.new(
                  conf['min_date']['year'].to_i,
                  conf['min_date']['month'].to_i,
                  conf['min_date']['day'].to_i
                )
    @max_date = Date.new(
                  conf['max_date']['year'].to_i,
                  conf['max_date']['month'].to_i,
                  conf['max_date']['day'].to_i
                )
    @min_time = {:hour => conf['min_hour'].to_i,
                 :minute => conf['min_minute'].to_i,
                 :second => conf['min_second'].to_i
                }
    @max_time = {:hour => conf['max_hour'].to_i,
                 :minute => conf['max_minute'].to_i,
                 :second => conf['max_second'].to_i
                }
    @use_date = conf['use_date'] 
    @use_date ||= "0"
    @use_time = conf['use_time'] 
    @use_time ||= "0"

    @delimitter = conf['datetime_delimitter']
  end

  def get_value

    if depend_on?
      res = data
    else
      res = {'date' => nil, 'time' => nil}
      if @use_date == "1"
        dt = get_rand_date(@min_date, @max_date)
        res['date'] = dt
      end

      if @use_time == "1"
        tm_s = get_rand_time(@min_time, @max_time)
        res['time'] = tm_s
      end
      set_data(res)
    end

    _res_array  = return_value(concat_datetime(res, @delimitter))
    _res_array.concat(get_child_values)

    return _res_array
  end

  def get_rand_date(min, max)
    days = max - min + 1
    return min + rand(days)
  end

  def get_second(tm)
    return tm['hour']*60*60 + tm['minute']*60 + tm['second']
  end

=begin
  def get_rand_time(min, max)
    span = max - min + 1
    res  = min + rand(span)
    res_div = res.divmod(3600)
    res_h   = res_div[0]
    res_div = res_div[1].divmod(60)
    res_m   = res_div[0]
    res_s   = res_div[1]
    return "#{sprintf('%02d',res_h)}:#{sprintf('%02d',res_m)}:#{sprintf('%02d', res_s)}"
  end
=end
  def get_rand_time(min, max)
    res_h = rand(max[:hour]-min[:hour]+1)+min[:hour]
    res_m = rand(max[:minute]-min[:minute]+1)+min[:minute]
    res_s = rand(max[:second]-min[:second]+1)+min[:second]
    return "#{sprintf('%02d',res_h)}:#{sprintf('%02d',res_m)}:#{sprintf('%02d', res_s)}"
  end

  def concat_datetime(hs, delm)
    res = Array.new
    res.push(hs['date'].strftime("%Y-%m-%d")) unless hs['date'].nil?
    res.push(hs['time']) unless hs['time'].nil?
    return res.join(delm)
  end
end
