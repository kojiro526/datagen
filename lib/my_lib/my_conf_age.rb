# -*- coding: utf-8 -*-
class MyLib::MyConfAge < MyLib::MyConfItem

  attr_reader :age_base_date

  def initialize(conf)
    super
    @age_base_date = Date.new(
                       conf['age_base_date']['year'].to_i,
                       conf['age_base_date']['month'].to_i,
                       conf['age_base_date']['day'].to_i
                     )
    @age_max = conf['age_max'].to_i
    @age_max ||= 120
    @age_min = conf['age_min'].to_i
    @age_min ||= 0
    if @age_max < @age_min
      tmp = @age_max
      @age_max = @age_min
      @age_min = tmp
    end
  end

  def get_value

    if depend_on?
      case data_status[:data_name]
        when 'datetime'
        # 日付時刻データを引き継いだ場合
          case @parent_item.class.name
            when 'MyLib::MyConfDateTime'
            when 'MyLib::MyConfAge'
              # 年齢アイテムを親に持つ場合、基準日も親のものを使用する。
              @age_base_date = @parent_item.age_base_date
          end

          # 満年齢計算
          age_tmp = 0
          date_tmp = data['date']
          while date_tmp <= (@age_base_date << 12)
            date_tmp = date_tmp >> 12
            age_tmp += 1
          end
          age = age_tmp
        when 'age'
          # 年齢アイテムに従属している場合は親のデータを参照。
          age = data
        else
          raise "data_status[:data_name]（#{data_status[:data_name]}）が不正です。"
      end
    else
      age = get_age(@age_min, @age_max)
      set_data(age)
    end
    age_s = sprintf("%d", age)

    _res_array  = return_value(age_s)
    _res_array.concat(get_child_values)

    return _res_array
  end

  def get_age(min, max)
    return min+rand(max-min)+1
  end

end
