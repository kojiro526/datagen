# -*- coding: utf-8 -*-
class MyLib::MyConfListConcat < Array

  # 与えられたアイテムが従属アイテムなら、すでに格納されたアイテムを走査する。
  # そうでなければpushする。
  def set_item(item)
    if self.size == 0
      self.push(item)
    else
      self[0].set_child_item(item)
    end
  end

  # my_conf_itemクラスのget_valueメソッドによって1カラムずつデータを取得する。
  def get_row(cols)
    row_hash = Array.new
    self.each{|item|
      # 2011-11-19 仕様変更によりget_valueは1つ以上のハッシュを含んだ配列を返す。
      # [{:seq => 'item01', :value => 'val1', :options =>{..}}, {:seq => ...} ]
      row_hash.concat(item.get_value(cols))
    }
    return row_hash
  end

  # フィールド名を各アイテムから再帰的に取得
  def get_field_names
    res = Array.new
    self.each{|item|
      # get_field_name は子アイテムを再帰的に呼び出し、[{:seq => 'item01', :name => 'colmun1'}, .. ]
      # を取得する。
      res.concat(item.get_field_name)
    }
    return res
  end

  # カラム名を各アイテムから再帰的に取得
  def get_column_names
    res = Array.new
    self.each{|item|
      # get_column_name は子アイテムを再帰的に呼び出し、[{:seq => 'item01', :name => 'colmun1'}, .. ]
      # を取得する。
      res.concat(item.get_column_name)
    }
    return res
  end

end
