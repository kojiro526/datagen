# -*- coding: utf-8 -*-
class MyLib::MyConfFullAddress < MyLib::MyConfAddress

  @@address_item_count = Town.count(:all)

  def initialize(conf)
    super
  end

  def get_value
    if depend_on?
    else
      town = Town.find(:first, :conditions => ["search_id=?", rand(@@address_item_count)], :include => [:prefecture, :city])
      town.generate_banchi
      set_data(town)
    end
    res = data[@@name_type[@kanji_kana_st]]
    _pref = data.prefecture[@@name_type[@kanji_kana_st]]
    _city = data.city[@@name_type[@kanji_kana_st]]
    _ban  = data.banchi[@@name_type[@kanji_kana_st]]
    res = _pref + _city + res + _ban
    res = conv_zenhan(res)

    _res_array  = return_value(res)
    _res_array.concat(get_child_values)

    return _res_array
  end
end
