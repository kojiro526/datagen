# -*- coding: utf-8 -*-
class MyLib::MyConfFullName < MyLib::MyConfName

  def initialize(conf)
    super
    @name_delimitter = conf['name_delimitter']
    @name_delimitter ||= '　'
  end

  def get_value

    res = Hash.new
    if depend_on?
      case data_status[:data_name]
        when 'family_name'
          res['fn'] = data
          res['gn'] = MyLib::MyConfGivenName::given_names.sample
        when 'full_name'
          res = data
        else
          raise "data_status[:data_name]（#{data_status[:data_name]}）が不正です。"
      end
    else
      res['fn'] = MyLib::MyConfFamilyName::family_names.sample
      res['gn'] = MyLib::MyConfGivenName::given_names.sample
      set_data(res)
    end
    res_fn = conv_zenhan(res['fn'][@@name_type[@kanji_kana_st]])
    res_gn = conv_zenhan(res['gn'][@@name_type[@kanji_kana_st]])
    res_fn = get_string_with_delimitter(res_fn)
    res_gn = get_string_with_delimitter(res_gn)

    _res_array  = return_value(res_fn + @name_delimitter + res_gn)
    _res_array.concat(get_child_values)

    return _res_array
  end

end
