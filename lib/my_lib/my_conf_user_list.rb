# -*- coding: utf-8 -*-
class MyLib::MyConfUserList < MyLib::MyConfItem

  def initialize(conf)
    super
    @user_list_view = conf['user_list_view']
    unless @user_list_view == 'value' || @user_list_view == 'label'
      @user_list_view = 'value' 
    end

    begin
      @user_list = Hash.new
      tmp = CSV::parse(conf['user_list'])
      tmp.each{|item|
        @user_list[item[0]] = item[1]
      }
    rescue
      @user_list = Array.new
    end
  end

  def data
    return @data
  end

  def data_status
    return @data_status
  end

  def get_value

    if depend_on?
      case @parent_item.data_status[:data_name]
        when 'user_list'
          tmp = @parent_item.data
          set_data(tmp)
        else
          arr_tmp = @user_list[@parent_item.data_result]
          if arr_tmp.blank?
            tmp = {:value => '', :label => ''} 
          else
            tmp = {:value => @parent_item.data_result, :label => arr_tmp} 
          end
          set_data(tmp)
      end
    else
      if @user_list.size > 0
        arr = @user_list.to_a
        arr_tmp = arr[rand(arr.size)]
        set_data({:value => arr_tmp[0], :label => arr_tmp[1]})
      else
        set_data({:value => '', :label => ''}) 
      end
      tmp = data
    end

    case @user_list_view
      when 'value'
        res = tmp[:value]
      when 'label'
        res = tmp[:label]
    end

    _res_array  = return_value(res)
    _res_array.concat(get_child_values)

    return _res_array
  end
end
