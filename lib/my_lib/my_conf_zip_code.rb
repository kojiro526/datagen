# -*- coding: utf-8 -*-
class MyLib::MyConfZipCode < MyLib::MyConfFullAddress

  @@delimitters = {'zip_del1' => '', 'zip_del2' => '-'}

  def initialize(conf)
    super
    @zip_delimitters = {'zip_del1' => conf['zip_del1'], 'zip_del2' => conf['zip_del2']}
  end

  def get_value

    if depend_on?
    else
      towns = Town.find(:all, :conditions => ["search_id=?", rand(@@address_item_count)])
      set_data(towns[0])
    end
    res = data.zip_code

    _param = get_checked_keys(@zip_delimitters)
	res = get_formatted_zipcode(res, @@delimitters[_param[rand(_param.size)]])

    _res_array  = return_value(res)
    _res_array.concat(get_child_values)

    return _res_array
  end

  def get_formatted_zipcode(zip, delimitter)
    return zip[0..2] + delimitter + zip[3..6]
  end
end
