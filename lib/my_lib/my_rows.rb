# -*- coding: utf-8 -*-
class MyLib::MyRows

  @@mime_list = {'csv' => 'text/csv', 'table' => 'text/html', 'sql' => 'text/plaie' }
  @@extensions = {'csv' => 'csv', 'table' => 'html', 'sql' => 'sql'}

  attr_reader :rows
  attr_accessor :field_names
  attr_accessor :column_names
  attr_accessor :configures

  def initialize
    @rows = Array.new
    @field_names = Array.new
    @column_names = Array.new
    @configures = Hash.new
  end

  def add_row(row)
    # row => [{:seq => 'item01', :value => 'val1'},{:seq => ...
    @rows.push(row)
  end

  def get_mime
    return @@mime_list[@configures[:data_format]]
  end

  def to_sql

    header_column_name = Array.new;
    @column_names.sort{|a, b| a[:seq]<=>b[:seq]}.each{|item|
      next unless item[:options]['common']['view']
      _name = ''
      case @configures[:sql][:column_qt]
        when '2'
          _name = '"' + item[:name] + '"'
        when '3'
          _name = "`#{item[:name]}`"
        else
          _name = item[:name]
      end
      header_column_name.push(_name)
    }
    _columns = header_column_name.join(', ')

    # テーブル名
    _table_name = ''
    case @configures[:sql][:column_qt]
      when '2'
        _table_name = '"' + @configures[:sql][:table_name] + '"'
      when '3'
        _table_name = "`#{@configures[:sql][:table_name]}`"
      else
        _table_name = @configures[:sql][:table_name]
    end

    _insert = "INSERT INTO #{_table_name} ("

    data = ''
    @rows.each{|row|
      _row = row.sort{|a, b|
        a[:seq] <=> b[:seq]
      }
      values = Array.new
      _row.each{|item|
        if item[:options]['sql']['qt']
          values.push("'#{item[:value]}'")
        else
          values.push(item[:value])
        end
      }
      data += _insert + _columns + ') VALUES (' + values.join(', ') + ");\n"
    }
    return data
  end

  def to_csv

    # ヘッダを取得
    header_field_name = Array.new;
    if @configures[:csv][:header_st]['field_name']
      @field_names.sort{|a, b| a[:seq]<=>b[:seq]}.each{|item|
        next unless item[:options]['common']['view']
        if item[:options]['csv']['qt']
          header_field_name.push('"'+item[:name]+'"')
        else
          header_field_name.push(item[:name])
        end
      }
    end

    header_column_name = Array.new;
    if @configures[:csv][:header_st]['column_name']
      @column_names.sort{|a, b| a[:seq]<=>b[:seq]}.each{|item|
        next unless item[:options]['common']['view']
        if item[:options]['csv']['qt']
          header_column_name.push('"'+item[:name]+'"')
        else
          header_column_name.push(item[:name])
        end
      }
    end
    data = ''
    data += header_field_name.size > 0 ? header_field_name.join(',')+"\n" : ''
    data += header_column_name.size > 0 ? header_column_name.join(',')+"\n" : ''

    # データを取得
    @rows.each{|row|
      _row = row.sort{|a,b|
        a[:seq] <=> b[:seq]
      }
      _array = Array.new
      _row.each{|item|
        next unless item[:options]['common']['view']
        if item[:options]['csv']['qt']
          _array.push('"'+item[:value]+'"')
        else
          _array.push(item[:value])
        end
      }
      data += _array.join(',') + "\n"
    }
    return data

  end

  def get_extension
    return @@extensions[@configures[:data_format]]
  end

end
