# -*- coding: utf-8 -*-
class MyLib::MyConfItem

    attr_accessor :item_id
    attr_accessor :item_seq
    attr_accessor :item_name
    attr_accessor :item_type
    attr_accessor :field_name
    attr_accessor :column_name
    attr_accessor :blank_ratio
    attr_writer   :data
    attr_writer   :data_status
    attr_accessor :data_result
    attr_reader   :depend_on
    attr_accessor :parent_item

  # paramsで渡されたitemごとの設定を引数としてオブジェクトを作る
  def initialize(conf)
    @item_id = conf['item_id']
    @item_seq = conf['item_seq']
    @item_name = conf['item_name']
    @item_type = conf['item_type']
    @field_name = conf['field_name']
    @column_name = conf['column_name']
    if conf.has_key?('depend_on')
      @depend_on = conf['depend_on']
    else
      @depend_on = ''
    end
    @blank_ratio = conf['blank_ratio']
    if @blank_ratio =~ /^[0-9].+$/
      @blank_ratio = @blank_ratio.to_i
      @blank_ratio = @blank_ratio > 100 ? 100 : @blank_ratio
    else
      @blank_ratio = 0
    end
    @child_items = Array.new
    @data = nil
    @data_status = nil
    @data_result = ''
    @parent_item = nil
    @prefix = conf['prefix']
    @prefix ||= ''
    @suffix = conf['suffix']
    @suffix ||= ''
    @op_by_format = parse_options(conf['options'])
  end

  def data
    if @parent_item.nil?
      return @data
    else
      return @parent_item.data
    end
  end

  def data_status
    if @parent_item.nil?
      return @data_status
    else
      return @parent_item.data_status
    end
  end

  # フィールドのタイプごとにオーバーライドする
  def get_value
#    return 'TEST'
     return return_value('TEST')
  end

  def get_field_name
    res = Array.new
    @child_items.each{|item|
      res.concat(item.get_field_name)
    }
    return res.concat([{:seq => @item_seq, :name => @field_name, :options => @op_by_format}])
  end

  def get_column_name
    res = Array.new
    @child_items.each{|item|
      res.concat(item.get_column_name)
    }
    return res.concat([{:seq => @item_seq, :name => @column_name, :options => @op_by_format}])
  end

  # 文字列の一文字ごとにchで指定の文字を挿入する
  def insert_chars(target, ch)
    _tmp = Array.new

    if target.blank?
      return target
    end

	target.chars{|chr|
      _tmp.push(chr)
    }
	res = _tmp.join(ch)
    return res
  end

  def return_value(value)
     # 空データかどうかを判定
     _blank = rand(100)+1
     _null  = false
     if 1 <= _blank && _blank <= @blank_ratio
       _null = true
     end

     _value = @prefix + value + @suffix

     @data_result = _value
     return [{:name => @item_name, :seq => @item_seq, :value => _value, :options => @op_by_format, :null => _null}]
  end

  # アイテムが依存関係にあるかどうかを判定する。
  def depend_on?
    return !@depend_on.blank?
  end

  # 当クラスを親とする子クラスであれば、子に追加する。
  # 当クラスの子でなければ、当クラスの子クラスに順に再帰的に渡していく。
  def set_child_item(item)
    if item.depend_on?
      if @item_id == item.depend_on
        # 親に該当するクラスだった場合、親のデータを渡してから子のリストに追加
#        item.data = @data
        @child_items.push(item)
        return true
      else
        @child_items.each{|ci|
          if ci.set_child_item(item)
            return true
          end
        }
      end
    end
    return false
  end

  # 半角を全角に変換
  def han2zen(str)
    return NKF::nkf("-WwX",str)
  end

  # 全角を半角に変換
  def zen2han(str)
    res = MyLib::MyTextFilter.zen_to_han(str)
    return res
  end

  # 全角カタカナを全角ひらがなに変換
  def kata2hira(str)
    return str.tr("ァ-ン", "ぁ-ん")
  end

  # 全角ひらがなを全角カタカナに変換
  def hira2kata(str)
    return str.tr("ぁ-ん","ァ-ン")
  end

  # チェックボックスから渡された配列から、値が1（チェック済み）を判定。
  # 該当するkeyを配列として返す。
  # 一件も該当しない場合は、1つ目を加える。
  def get_checked_keys(h)
    _param = Array.new
    h.each{|key, val|
      if val == "1"
        _param.push(key)
      end
    }
    if _param.size == 0
      # 最初のキーを設定する。
      _param.push(h.keys.sort{|a,b| a<=>b}[0])
    end
    return _param
  end

  # 子アイテムがある場合は子アイテムのget_valueメソッドを通じて再帰的にデータを取得。
  def get_child_values
    _res_array = Array.new
    @child_items.each{|item|
      item.parent_item = self # 子アイテムに親アイテムへの参照を設定する
      _res_array.concat(item.get_value)
    }
    return _res_array
  end

  def parse_options(params)
    res = Hash.new
    res['common'] = Hash.new
    res['common']['view'] = false
    res['csv'] = Hash.new
    res['csv']['qt'] = false
    res['sql'] = Hash.new
    res['sql']['qt'] = false

    if params.blank?
      return res
    end

    if params.keys.include?('common')
      if params['common'].keys.include?('view')
        if params['common']['view'] == '1'
          res['common']['view'] = true
        end 
      end
    end

    if params.keys.include?('csv')
      if params['csv'].keys.include?('qt')
        if params['csv']['qt'] == '1'
          res['csv']['qt'] = true
        end 
      end
    end

    if params.keys.include?('sql')
      if params['sql'].keys.include?('qt')
        if params['sql']['qt'] == '1'
          res['sql']['qt'] = true
        end 
      end
    end

    return res
  end

  # 親から引き継いだデータが何であるかを判断するための情報を取得する。
  # 1. 年齢１←（従属）←年齢２
  # 2. 日付時刻←（従属）←年齢１←（従属）←年齢２
  # 上記の場合、年齢２は日付アイテムのデータと年齢アイテムのデータを
  # 両方引き継ぐ可能性がある。
  # それを判断するための情報を提供する。
  def get_parent_data_status(pd)
    hs = Hash.new
    hs[:data_class_name] = pd.class.name
    hs[:provider_class_name] = ''
    hs[:provider_class_name] = ''
  end

  def set_data(dt)
    @data = dt
    hs = Hash.new
    hs[:data_class_name] = dt.class.name
    hs[:data_name] = @item_type
    @data_status = hs
  end
end
