# -*- coding: utf-8 -*-
class MyLib::MyConfGivenName < MyLib::MyConfName

  @@given_names = GivenName.all

  def initialize(conf)
    super
  end

  def get_value

    if depend_on?
      case data_status[:data_name]
        when 'given_name'
          res = data[@@name_type[@kanji_kana_st]]
        when 'full_name'
          res = data['gn'][@@name_type[@kanji_kana_st]]
        else
          raise "data_status[:data_name]（#{data_status[:data_name]}）が不正です。"
      end
    else
      set_data(@@given_names.sample)
      res = data[@@name_type[@kanji_kana_st]]
    end
    res = conv_zenhan(res)
    res = get_string_with_delimitter(res)

    _res_array  = return_value(res)
    _res_array.concat(get_child_values)

    return _res_array
  end

  def self.given_names
    return @@given_names
  end

end
