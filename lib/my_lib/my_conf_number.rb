# -*- coding: utf-8 -*-
class MyLib::MyConfNumber < MyLib::MyConfItem


  def initialize(conf)
    super
    @number_mode = conf['number_mode']
    unless @number_mode == '1' || @number_mode == '2'
      @number_mode = '1'
    end

    case @number_mode
      when '1'
        @init_number = conf['init_number'].to_i
        @init_number ||= 1
        @max_number = conf['max_number'].to_i
        @max_number ||= 0
        @step_number = conf['step_number'].to_i
        @step_number ||= 1
        @stat_num = @init_number
        @stat_num ||= 0
      when '2'
        @init_number = conf['init_number_r'].to_i
        @init_number ||= 0
        @max_number = conf['max_number_r'].to_i
        @max_number ||= 9999
      else
        raise 'Bad options!'
    end
    @zerofill_max = conf['zerofill_max'].to_i
  end

  def get_value

    if depend_on?
    else
      case @number_mode 
        when '1'
          num = @stat_num
          @stat_num += @step_number
          if @max_number > 0 && @stat_num > @max_number
            @stat_num = @init_number
          end
        when '2'
          num = rand(@max_number - @init_number + 1) + @init_number
      end
      num = sprintf("%0#{@zerofill_max}d", num)
      set_data(num)
    end
    num = data

    _res_array  = return_value(num)
    _res_array.concat(get_child_values)

    return _res_array
  end

end
