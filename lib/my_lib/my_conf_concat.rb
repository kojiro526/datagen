# -*- coding: utf-8 -*-
class MyLib::MyConfConcat < MyLib::MyConfItem

  attr_reader :item1_id
  attr_reader :item2_id
  attr_reader :item1
  attr_reader :item2

  def initialize(conf)
    super
    @item1_id = conf['concat_item1']
    @item2_id = conf['concat_item2']
    @item1 = {:item_id => nil, :item_name => nil, :item_type => nil }
    @item2 = {:item_id => nil, :item_name => nil, :item_type => nil }
    @concat_delimitter = conf['concat_delimitter']
  end

  def get_value(cols)

    concat_item = {:item1 => '', :item2 => ''}
    cols.each{|col|
      if col[:name] == @item1[:item_name]
        concat_item[:item1] = col[:value]
      end

      if col[:name] == @item2[:item_name]
        concat_item[:item2] = col[:value]
      end
    }

    res = concat_item[:item1] + @concat_delimitter + concat_item[:item2]

    # 子アイテムがある場合は子アイテムのget_valueメソッドを通じて再帰的にデータを取得。
    _res_array = get_child_values(cols)

    return _res_array.concat(return_value(res))
  end

  def set_item1(item)
    @item1 = {:item_id => item.item_id, :item_name => item.item_name, :item_type => item.item_type }
  end

  def set_item2(item)
    @item2 = {:item_id => item.item_id, :item_name => item.item_name, :item_type => item.item_type }
  end

  def set_child_item(item)
    if @child_items.size == 0
      @child_items.push(item)
    else
      @child_items[0].set_child_item(item)
    end
  end

  # 子アイテムがある場合は子アイテムのget_valueメソッドを通じて再帰的にデータを取得。
  def get_child_values(row)
    _res_array = Array.new
    @child_items.each{|item|
      item.parent_item = self # 子アイテムに親アイテムへの参照を設定する
      _res_array.concat(item.get_value(row))
    }
    return _res_array
  end

end
