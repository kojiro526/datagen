class CreatePrefectures < ActiveRecord::Migration
  def self.up
    create_table :prefectures do |t|
      t.string :pref_code, :limit => 2
      t.string :pref_code_iso, :limit => 10
      t.string :name, :limit => 20, :null => false
      t.string :pronounce, :limit => 50
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end

  def self.down
    drop_table :prefectures
  end
end
