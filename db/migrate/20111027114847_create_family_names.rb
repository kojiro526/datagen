class CreateFamilyNames < ActiveRecord::Migration
  def self.up
    create_table :family_names do |t|
      t.string :name, :null => false, :limit => 20
      t.string :pronunciation, :null => false, :limit => 20
      t.string :sex, :limit => 1
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end

  def self.down
    drop_table :family_names
  end
end
