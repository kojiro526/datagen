class CreateGivenNames < ActiveRecord::Migration
  def self.up
    create_table :given_names do |t|
      t.string :name, :null => false, :limit => 20
      t.string :pronunciation, :null => false, :limit => 20
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end

  def self.down
    drop_table :given_names
  end
end
