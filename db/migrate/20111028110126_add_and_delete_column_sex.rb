class AddAndDeleteColumnSex < ActiveRecord::Migration
  def self.up
    remove_column :family_names, :sex
    add_column    :given_names,  :sex, :string, :limit => 1
  end

  def self.down
    remove_column :given_names, :sex
    add_column    :family_names,:sex, :string, :limit => 1
  end
end
