class UpdatePrefectures < ActiveRecord::Migration
  def self.up
    rename_column :prefectures, :pronounce, :pronunciation
    add_index :prefectures, [:pref_code], :name=>"pref_code_unique", :unique=>true
  end

  def self.down
    rename_column :prefectures, :pronunciation, :pronounce
    remove_index :prefectures, :name=>"pref_code_unique"
  end
end
