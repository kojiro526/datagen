class CreateTowns < ActiveRecord::Migration
  def self.up
    create_table :towns do |t|
      t.integer :search_id, :null => false
      t.string :pref_code,  :limit => 2, :null => false
      t.string :city_code,  :limit => 5, :null => false
      t.string :zip_code,   :limit => 7, :null => false
      t.string :name,          :limit => 100, :null => false
      t.string :pronunciation, :limit => 100
      t.integer :st1, :default => 0
      t.integer :st2, :default => 0
      t.integer :st3, :default => 0
      t.integer :st4, :default => 0
      t.integer :st5, :default => 0
      t.integer :st6, :default => 0
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
    add_index :towns, ["search_id"], :name => "towns_search_id_unique", :unique => true
    add_index :towns, ["pref_code"], :name => "towns_pref_code"
    add_index :towns, ["city_code"], :name => "towns_city_code"
  end

  def self.down
    drop_table :towns
  end
end
