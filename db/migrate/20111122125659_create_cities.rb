class CreateCities < ActiveRecord::Migration
  def self.up
    create_table :cities do |t|
      t.string :pref_code, :limit => 2, :null => false
      t.string :city_code, :limit => 5, :null => false
      t.string :city_code_cd, :limit => 6, :null => false
      t.string :name,         :limit => 50, :null => false
      t.string :pronunciation,:limit => 100
      t.string :telephone,    :limit => 10
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
    add_index :cities, ["city_code"], :name => "city_code_unique", :unique => true
  end

  def self.down
    drop_table :cities
  end
end
