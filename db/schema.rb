# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20111123082734) do

  create_table "cities", :force => true do |t|
    t.string   "pref_code",     :limit => 2,   :default => "", :null => false
    t.string   "city_code",     :limit => 5,   :default => "", :null => false
    t.string   "city_code_cd",  :limit => 6,   :default => "", :null => false
    t.string   "name",          :limit => 50,  :default => "", :null => false
    t.string   "pronunciation", :limit => 100
    t.string   "telephone",     :limit => 10
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  add_index "cities", ["city_code"], :name => "city_code_unique", :unique => true

  create_table "family_names", :force => true do |t|
    t.string   "name",          :limit => 20, :default => "", :null => false
    t.string   "pronunciation", :limit => 20, :default => "", :null => false
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

  create_table "given_names", :force => true do |t|
    t.string   "name",          :limit => 20, :default => "", :null => false
    t.string   "pronunciation", :limit => 20, :default => "", :null => false
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "sex",           :limit => 1
  end

  create_table "prefectures", :force => true do |t|
    t.string   "pref_code",     :limit => 2
    t.string   "pref_code_iso", :limit => 10
    t.string   "name",          :limit => 20, :default => "", :null => false
    t.string   "pronunciation", :limit => 50
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

  add_index "prefectures", ["pref_code"], :name => "pref_code_unique", :unique => true

  create_table "towns", :force => true do |t|
    t.integer  "search_id",                                    :null => false
    t.string   "pref_code",     :limit => 2,   :default => "", :null => false
    t.string   "city_code",     :limit => 5,   :default => "", :null => false
    t.string   "zip_code",      :limit => 7,   :default => "", :null => false
    t.string   "name",          :limit => 100, :default => "", :null => false
    t.string   "pronunciation", :limit => 100
    t.integer  "st1",                          :default => 0
    t.integer  "st2",                          :default => 0
    t.integer  "st3",                          :default => 0
    t.integer  "st4",                          :default => 0
    t.integer  "st5",                          :default => 0
    t.integer  "st6",                          :default => 0
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  add_index "towns", ["city_code"], :name => "towns_city_code"
  add_index "towns", ["pref_code"], :name => "towns_pref_code"
  add_index "towns", ["search_id"], :name => "towns_search_id_unique", :unique => true

end
