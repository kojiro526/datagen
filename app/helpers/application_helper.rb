# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  def select_time(def_h, def_m, def_s, prefix)
    res_h = "<select name=\"#{prefix}[hour]\" class=\"width4em\">\n"
    0.upto(23){|i|
      res_h += "<option value=#{i}>#{i}</option>\n"
    }
    res_h += "</select>\n"

    res_m = "<select name=\"#{prefix}[minute]\" class=\"width4em\">\n"
    0.upto(59){|i|
      res_m += "<option value=#{i}>#{i}</option>\n"
    }
    res_m += "</select>\n"

    res_s = "<select name=\"#{prefix}[second]\" class=\"width4em\">"
    0.upto(59){|i|
      res_s += "<option value=#{i}>#{i}</option>"
    }
    res_s += "</select>\n"

    return res_h + ':' + res_m + ':' + res_s
  end

  def select_hour(def_h, prefix)
    res = "<select name=\"#{prefix}\" class=\"width4em\">\n"
    0.upto(23){|i|
      res += "<option value=#{i}>#{i}</option>\n"
    }
    res += "</select>\n"
    return res
  end

  def select_minute(def_m, prefix)
    res = "<select name=\"#{prefix}\" class=\"width4em\">\n"
    0.upto(59){|i|
      res += "<option value=#{i}>#{i}</option>\n"
    }
    res += "</select>\n"
    return res
  end

  def select_second(def_s, prefix)
    res = "<select name=\"#{prefix}\" class=\"width4em\">"
    0.upto(59){|i|
      res += "<option value=#{i}>#{i}</option>"
    }
    res += "</select>\n"
    return res
  end
end
