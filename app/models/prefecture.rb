# -*- coding: utf-8 -*-
class Prefecture < ActiveRecord::Base
  set_primary_key 'pref_code'
  has_many :cities, :foreign_key => 'pref_code'
  has_many :towns,  :foreign_key => 'pref_code'
end
