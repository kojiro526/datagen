# -*- coding: utf-8 -*-
class City < ActiveRecord::Base
  set_primary_key 'city_code'
  has_many :towns, :foreign_key => "city_code"
end
