# -*- coding: utf-8 -*-
class Town < ActiveRecord::Base
  belongs_to :prefecture, :foreign_key => "pref_code"
  belongs_to :city, :foreign_key => "city_code"

  def generate_banchi
    @banchi = ''
    cho_me = sprintf("%d",rand(9)+1)
    ban_chi = sprintf("%d",rand(20)+1)
    goh = sprintf("%d",rand(50)+1)

    mode = rand(2)+1
    tmp = Hash.new
    case mode
      when 1
        han = cho_me + '-' + ban_chi + '-' + goh
        zen = han.tr("0-9\-","０-９－")
        tmp[:name] = zen
        tmp[:pronunciation] = zen
      when 2
        han = cho_me + '-' + ban_chi
        zen = han.tr("0-9\-","０-９－")
        tmp[:name] = zen
        tmp[:pronunciation] = zen
    end
    @banchi = tmp
  end

  def banchi
    return @banchi
  end
end
