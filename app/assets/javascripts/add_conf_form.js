$(document).ready(function(){
$("div.validation_error").css("display", "none");
$("div.loading_image").hide();
//$("#data_table").hide();

$("#form_config").validate({
    ignore: "",
    invalidHandler: function(form, validator) {
      var errors = validator.numberOfInvalids();
      if (errors) {
        var message = '入力された値に' + errors + '件のエラーがあります。'
        $("div.validation_error span").html(message);
        $("div.validation_error").show();
      } else {
        $("div.validation_error").hide();
      }
    }
 });

  allOptionsInvisible();

  $("options_box > #table_option").css("display", "block")

  //出力形式のプルダウンを変えるごとに設定内容を切り替える。
  $("#select_data_format").change(function () {
    allOptionsInvisible();  
    var str = "";
    str = $("#select_data_format option:selected:first").text();
    val = $("#select_data_format option:selected:first").val();
    switch(val){
      case 'table':
        $("#table_option").css("display","block");
        break;
      case 'csv':
        $("#csv_option").css("display","block");
        break;
      case 'sql':
        $("#sql_option").css("display","block");
        break;
      default:
        break;
    }
  }).change();

});

function showLoading(){
  $("div.loading_image").show();
}
function hideLoading(){
  $("div.loading_image").hide();
}

function validateForm(){
  if($("#form_config").valid()){
    $("div.validation_error").hide();
    return true;
  }else{
    return false;
  }
};

function allOptionsInvisible(){
  $("#options_box > dl.option_container").each(function(){
    $(this).css("display", "none");
  });
};

//設定ファイルに従ってアイテムを追加
function addItemByConfig(conf){

  if(conf === null || conf === undefined){
    return;
  }

  //設定ファイルのItem単位に処理
  $.each(conf, function(){
    //テンプレートからアイテムを生成
    generateItem(this.field_name, this.item_type, this.item_id, this.item_name);
    //設定ファイルの値でフォームを上書き
    updateItemValues(this, conf);
  });


  renumbering();

}

//設定ファイルの値でアイテムフォームを上書き
function updateItemValues(item, conf){

  //アイテムの内容を取得
  $("div#" + item.item_id + " :input").each(function(){
    setValueToFormElement(this, conf);
  });

}
//設定ファイルの値で全体設定フォームを上書き
function updateGeneralConfigValues(genconf){

  //アイテムの内容を取得
  $("#operation_box :input").each(function(){
    setValueToFormElement(this, genconf);
  });

}

//form_element: 値を復元する先となるFormの要素。
//val_json: フォームの内容を元に生成されたJSONオブジェクト。
//          設定ファイルから読み込まれた値であることを想定。
function setValueToFormElement(form_element, val_json){

    var node_name = $(form_element)[0].nodeName.toLowerCase();
    switch(node_name){
      case 'input':
        switch($(form_element).attr('type')){
          case 'text':
          case 'hidden':
            var val_tmp = getValueByName($(form_element).attr('name'), val_json);
            if(val_tmp===null){
              val_tmp = "";
            }else if(val_tmp===undefined){
              //デフォルト値を入れる
              val_tmp = "";
            };
            $(form_element).val(val_tmp);
            break;
          case 'radio':
          case 'checkbox':
            var val_tmp = getValueByName($(form_element).attr('name'), val_json);
            if(val_tmp === null){
              var array_tmp = [];
            }else if(val_tmp === undefined){
              //デフォルト値を入れる
              val_tmp = "";
            }else{
              var array_tmp = [val_tmp];
            };
            $(form_element).val(array_tmp);
            break;
          default:
            break;
        };
        break;
      case 'textarea':
        var val_tmp = getValueByName($(form_element).attr('name'), val_json);
        if(val_tmp===null){
          val_tmp = "";
        }else if(val_tmp===undefined){
          //デフォルト値を入れる
              val_tmp = "";
        };
        $(form_element).val(val_tmp);
        break;
      case 'select':
        var val_tmp = getValueByName($(form_element).attr('name'), val_json);
        if(val_tmp===null){
          val_tmp = "";
        }else if(val_tmp===undefined){
          //デフォルト値を入れる
              val_tmp = "";
        };
        $(form_element).val(val_tmp);
        break;
      default:
        break;
    };

}

//引数で与えられた、要素のname属性から、対応する設定ファイルの値を読み取って返す。
//ex) conf[aaa][bbb] -> item[bbb]
//attr_name: 要素のname属性 ex) conf[xxxxx][yyyyy]
//return: value or null or undefined
//  null: チェックボックスなどの値がセットされていない状態はnullとなる。
//  undefined: 設定ファイルの中に項目が無い場合はundefinedとなる。
function getValueByName(attr_name, item){

  //name属性（conf[xxxxx][yyyyy]）から添え字を抽出
  var re = /\[(.*?)\]/g;
  var substrs = new Array();
  while(substr = re.exec(attr_name)){
    substrs.push(substr[1]);
  }

  //JSONオブジェクトから添え字を指定して値を取得
  //オブジェクトは参照渡しになるので、$.extendでコピーする。
  //var item_tmp = $.extend(true, item); //このやり方はNG
  var item_tmp = clone(item);
  for(var i=0; i < substrs.length; i++){
    //checkboxなどはオフにすると設定ファイルにプロパティ自体が残らないのでまず存在チェック
    if(substrs[i] in item_tmp){
      item_tmp = item_tmp[substrs[i]];
    }else{
//      return null;
      return undefined;
    };
  };
  return item_tmp;

}

//アイテム欄からアイテムを追加
function addItem(){
  var item_count = $("#item_configs > div.item").size();
  if (item_count > 99 ){
    alert("これ以上は追加できません");
    return;
  };

  var selected_count = $("#item_list option:selected").size();
  if( selected_count > 1 ){
    alert("アイテムは一つだけ選択してください。");
    return;
  };
  if( selected_count == 0 ){
    alert("アイテムを選択してください。");
    return;
  };

  var selected_text = "";
  var selected_value = "";
  selected_text = $("#item_list").children(":selected").text();
  selected_value = $("#item_list").children(":selected").val();
  dd = new Date();
  item_id    = dd.getTime(); //一意の値を生成
  item_name  = 'item'+("0"+(item_count+1)).slice(-2); //item01..99の文字列を生成

  //テンプレートからアイテムを生成
  generateItem(selected_text, selected_value, item_id, item_name);

  renumbering();
};

//テンプレートをアペンド
function generateItem(selected_text, selected_value, item_id, item_name){

  //テンプレートからフィールド設定用のHTMLを取得
  var field_values = [{
    item_id: item_id,
    item_seq: item_name,
    item_name: item_name,
    item_type: selected_value,
    selected_value: selected_value,
    field_name: selected_text,
    column_name: selected_value,
    blank_ratio: 0
  }];
  var item_type_list = [
    'family_name',
    'given_name',
    'full_name',
    'number',
    'age',
    'telephone',
    'address',
    'datetime',
    'prefecture',
    'zip_code',
    'sex',
    'user_list'
  ];
  var sub_field_values = [{item_id: item_id}];

  //共通設定テンプレートをアペンド
  $( "#configItemTemplate").tmpl(field_values).appendTo( "#item_configs" );

  //アイテムの種類毎にテンプレートをアペンド
  var append_selector = "#item_configs > #"+item_id+" div.item_options";
  switch(selected_value){
    case 'family_name':
      var dlist = ['family_name', 'full_name'];
      $.extend(sub_field_values[0],
        { add_space1: true, add_space2: false, depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configNameTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configHanKanTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'given_name':
      var dlist = ['given_name', 'full_name'];
      $.extend(sub_field_values[0],
        { depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configNameTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configHanKanTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'full_name':
      var dlist = ['full_name', 'family_name'];
      $.extend(sub_field_values[0],
        { name_delimitter: '　', depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configNameTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configHanKanTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configFullNameTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'number':
      var dlist = ['number'];
      $.extend(sub_field_values[0],
        { init_number: 1, step_number: 1, max_number: 0, init_number_r:0, max_number_r:99999, zerofill_max: 0, depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configNumberTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'age':
      var dlist = ['age', 'datetime'];
      $.extend(sub_field_values[0],
        { age_min: 0, age_max: 120, depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configAgeTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'telephone':
      var dlist = ['telephone'];
      $.extend(sub_field_values[0],
        { init_number: 1, step_number: 1, zerofill_max: 0, depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configTelTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'address':
      var dlist = ['address', 'zip_code'];
      $.extend(sub_field_values[0],
        { depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configHanKanTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'datetime':
      var dlist = ['datetime'];
      $.extend(sub_field_values[0],
        { datetime_delimitter: ' ', depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configDateTimeTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'prefecture':
      var dlist = ['prefecture', 'address', 'zip_code'];
      $.extend(sub_field_values[0],
        { depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configHanKanTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'zip_code':
      var dlist = ['zip_code'];
      $.extend(sub_field_values[0],
        { depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configZipCodeTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'sex':
      var dlist = ['sex','given_name','full_name'];
      $.extend(sub_field_values[0],
        { sex_label1: 1, sex_label2: 2, depend_list: dlist }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configSexTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'user_list':
      $.extend(sub_field_values[0],
        { depend_list: item_type_list }
      );
      $( "#configDependOnTemplate").tmpl(sub_field_values).appendTo( append_selector );
      $( "#configUserListTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    case 'concat':
      $.extend(sub_field_values[0],
        { item_type_list: item_type_list }
      );
      $( "#configConcatTemplate").tmpl(sub_field_values).appendTo( append_selector );
      break;
    default:
      break;
  }; //switch

  switch(selected_value){
    case 'concat':
      setConcatWithToSelect($("#"+item_id), item_type_list);
      break;
    case 'user_list':
      setConcatWithToSelect($("#"+item_id), item_type_list);
      break;
    default:
      setDependOnToSelect($("#"+item_id), dlist);
      break;
  };
  $("#"+item_id+" > div.item_body").css("display","none");

  setItemHeader($("#"+item_id));

};

//アイテムを削除
function removeItem(target_name){
  var obj = $(target_name).parent("div.item_header_function").parent("div.item_header").parent("div.item");
//  var res = confirm(target_name.id+"を削除しますか？");
  var item_name = obj.find("input.item_name:first").val();
  var field_name = obj.find("input.field_name:first").val();
  var res = confirm(item_name+":"+field_name+" を削除しますか？");
  if (!res){return;};
  obj.remove();
//  $(target_name).parent("div.item_header_function").parent("div.item_header").parent("div").remove();

  //番号を振りなおし
  renumbering();
};

function renumbering(){
  //番号を振りなおし
  var i = 1;
  var new_id = '';
  $("#item_configs > div.item").each(function(){
    new_name = 'item'+("0"+i).slice(-2);
    seq_id = 'seq'+("0"+i).slice(-2);
/*
// DEL 2014-03-30 実際には何もreplaceしていない。
// undefineオブジェクトの時にreplaceメソッドが無いというエラーが出るのでコメントアウト。
    $(this).find("input").each(function(){
alert($(this).attr('id'));
		$(this).attr('id', $(this).attr('id').replace(/item[0-9][0-9]/, new_name));
alert($(this).attr('name'));
		$(this).attr('name', $(this).attr('name').replace(/item[0-9][0-9]/, new_name));
	});
*/
    $(this).find("input.item_seq:first").val(seq_id);
    $(this).find("input.item_name:first").val(new_name);
    $(this).attr('name', new_name);
    setItemHeader(this);
    i++;
  });

  //従属設定のリストボックスを再設定
  $("#item_configs > div.item").filter(function(index){
    // 従属設定のリストが一つ以上あるものをフィルタリング
    return $(this).find("select.depend_on").length > 0
  }).each(function(){
    var item_type_list = new Array();
    $(this).find("input.depend_type").each(function(){
      item_type_list.push($(this).val());
    });

    // 現在選択されているアイテムのitem_idを待避
    var select = $(this).find("select.depend_on:first"); 
    var selectedIndex = select[0].selectedIndex;
    var selected_id = '';
    if (selectedIndex > 0){
      selected_id = select[0].options[selectedIndex].value;
    }

    // リストボックスをリフレッシュ
    setDependOnToSelect(this, item_type_list);

    // 待避していたitem_idを元にリストボックスの選択状態を復元
    var select2 = $(this).find("select.depend_on:first"); 
    var fg = false;
    for (i=0; i < select2[0].length; i++){
      if (select2[0].options[i].value == selected_id){
        select2[0].selectedIndex = i;
        fg = true;
        break;
      }
    };
    // 選択していたitem_idが無くなった場合は1番目を選択状態にする。
    if (!fg){
      select2[0].selectedIndex = 1;
    };
    
  });

  //連結設定のリストボックスを再設定
  $("#item_configs > div.item").filter(function(index){
    // 連結設定のリストが一つ以上あるものをフィルタリング
    return $(this).find("select.concat_with").length > 0
  }).each(function(){
    var this_item = $(this);
    var item_type_list = new Array();
    $(this).find("input.concat_type").each(function(){
      item_type_list.push($(this).val());
    });

    // 連結リストごとの処理
    $(this).find("select.concat_with").each(function(){
    // 現在選択されているアイテムのitem_idを待避
      var selectedIndex = this.selectedIndex;
//alert('selectedIndex:' + selectedIndex);
      var selected_id = '';
      if (selectedIndex > 0){
        selected_id = this.options[selectedIndex].value;
      }
//alert('selected_id:' + selected_id);
      // リストボックスをリフレッシュ
//      setConcatWithToSelect(this_item, item_type_list);
      var data = getDependOnList(this_item, item_type_list);
      setConcatWithToSelect2(this, data);

      // 待避していたitem_idを元にリストボックスの選択状態を復元
      var fg = false;
      for (i=0; i < this.length; i++){
        if (this.options[i].value == selected_id){
//alert('options:' + this.options[i].text);
          this.selectedIndex = i;
          fg = true;
          break;
        }
      };
      // 選択していたitem_idが無くなった場合は1番目を選択状態にする。
      if (!fg){
        $(this).selectedIndex = 1;
      };
    });
    
  });

  // UP,DOWNボタンの非活性化
  i = 1;
  $("#item_configs > div").each(function(){
    if (i==1){
      $(this).find("input.up_button").attr('disabled',true);
      $(this).find("input.down_button").attr('disabled',false);
    }else{
      $(this).find("input.up_button").attr('disabled',false);
      $(this).find("input.down_button").attr('disabled',false);
    };
    i++;
  });
  $("#item_configs > div.item:last").find("input.down_button").attr('disabled', true);
};

//アイテムウインドウの開閉
function itemWindowToggle(obj){
  var toggle = $(obj).parent("div.item_header").next("div.item_body").css("display");
  var st = "none";
  if(toggle=="none"){
    st = "block";
  }else{
    st = "none";
  }
  $(obj).parent("div.item_header").next("div.item_body").css("display",st);
};
//アイテムを一つ上に移動
function moveUp(obj){
  var current_item = $(obj).parent("div.item_header_function").parent("div.item_header").parent("div.item");
  var target_item = $(current_item).prev();
  $(current_item).insertBefore(target_item);

  renumbering();
};
//アイテムを一つ下に移動
function moveDown(obj){
  var current_item = $(obj).parent("div.item_header_function").parent("div.item_header").parent("div.item");
  var target_item = $(current_item).next();
  $(current_item).insertAfter(target_item);

  renumbering();
};

// 従属対象となるitem_typeの一覧を取得する。
function getDependOnList(obj, names){
  var current_id = $(obj).find("input.item_id:first").val();
  res = new Array()
  $("#item_configs > div.item").each(function(){
    var item_id = $(this).find("input.item_id:first").val();
    if (item_id != current_id){
      var item_name = $(this).find("input.item_name:first").val();
      var item_type = $(this).find("input.item_type:first").val();
      var field_name = $(this).find("input.field_name:first").val();
      // 該当アイテムのitem_typeがnameで指定した物であれば従属リストに入れる。
      if (jQuery.inArray(item_type, names)>=0){
        res.push(new Array(item_id, item_name + ':' + field_name));
      };
    };
  });
  return res;
};

function setDependOnToSelect(obj, depend_list){
  var data = getDependOnList(obj, depend_list);
  $(obj).find("select.depend_on").each(function(){
    //初期化
    for(i=1; i <= this.length; i++){
      this.options[i] = null;
    };
    var count = 1;
    cout = count + 1;
    for(i=0; i < data.length; i++){
      this.options[count] = new Option(data[i][1], data[i][0]);
      cout = count + 1;
    };

    //optionが無い場合は無効化。
    if(this.length > 0){
      $(this).attr('disabled', false);
    }else{
      $(this).attr('disabled', true);
    };
  });
  
};

function setConcatWithToSelect(obj, concat_list){
  var data = getDependOnList(obj, concat_list);
  $(obj).find("select.concat_with").each(function(){
/*
    //初期化
    for(i=1; i <= this.length; i++){
      this.options[i] = null;
    };
    var count = 1;
    this.options[count] = new Option("","");
    for(i=0; i < data.length; i++){
      this.options[count++] = new Option(data[i][1], data[i][0]);
    };
*/
    setConcatWithToSelect2(this, data);
  });
};

function setConcatWithToSelect2(obj, data){
  //初期化
  for(i=1; i <= obj.length; i++){
    obj.options[i] = null;
  };
  var count = 1;
  obj.options[count] = new Option("","");
  for(i=0; i < data.length; i++){
    obj.options[count++] = new Option(data[i][1], data[i][0]);
  };
}

function setItemHeader(obj){
  var item_name = $(obj).find("input.item_name:first").val();
  var item_type_kj = $(obj).find("input.field_name:first").val();
  $(obj).find("span.item_header_id:first").text('ID:'+item_name+' '+item_type_kj);
};
function setItemHeaderById(item_id){
  setItemHeader($("#"+item_id));
};

function closeAll(){
  $("#item_configs > div.item").find("div.item_body").each(function(){
    $(this).css("display", "none");
  });
};
function openAll(){
  $("#item_configs > div.item").find("div.item_body").each(function(){
    $(this).css("display", "block");
  });
};

// ダウンロード用の隠しフォームを生成してPOST
function dataDownload(){

  if($("#form_for_download")){ $("#form_for_download").remove(); };

  $( "<form/>", {
    "id": "form_for_download",
    "action": "generate_data/download",
    "accept-charset": "UTF-8",
    "method": "post" 
  }).appendTo("body");

  $("#form_for_download").css('display', 'none');

  //全体設定の内容を取得
  $("#operation_box :input").each(function(){
    appendDummyElements("#form_for_download", $(this));
  });

  //アイテムの内容を取得
  $("#item_configs > div.item").each(function(){
    //":input"によってinput,select,textareaを取得
    $(this).find(":input").each(function(){
      appendDummyElements("#form_for_download", $(this));
    });
  });

  $("#form_for_download").submit();

};

//設定を設定ファイルにダウンロード
function configDownload(){

  if($("#form_for_config_download")){ $("#form_for_config_download").remove(); };

  var file_name = $('#download_config_filename').val();

  $( "<form/>", {
    "id": "form_for_config_download",
    "action": "generate_data/config_download",
    "accept-charset": "UTF-8",
    "method": "post" 
  }).appendTo("body");

  $("#form_for_config_download").css('display', 'none');

  var input_filename = document.createElement('input');
  $(input_filename).attr('type', 'hidden');
  $(input_filename).attr('name', 'download_filename');
  $(input_filename).val(file_name);
  $("#form_for_config_download")[0].appendChild(input_filename);
  

  //全体設定の内容を取得
  $("#operation_box :input").each(function(){
    appendDummyElements("#form_for_config_download", $(this));
  });

  //アイテムの内容を取得
  $("#item_configs > div.item").each(function(){
    //":input"によってinput,select,textareaを取得
    $(this).find(":input").each(function(){
      appendDummyElements("#form_for_config_download", $(this));
    });
  });

  $("#form_for_config_download").submit();

};

//元になる要素から必要な値をダミーフォームへ追加。
function appendDummyElements(target_id, source_element){
  var node_name = source_element[0].nodeName.toLowerCase();
  switch(node_name){
    case 'input':
      copyInput(target_id, source_element);
      break;
    case 'textarea':
      copyTextarea(target_id, source_element);
      break;
    case 'select':
      copySelect(target_id, source_element);
      break;
    default:
      break;
  };
};

function copyInput(target_form, source_input){
  var input = document.createElement('input');
  $(input).attr('type', source_input.attr('type'));
  $(input).attr('name', source_input.attr('name')); 
  $(input).attr('value', source_input.attr('value')); 
  switch($(input).attr('type')){
    case 'checkbox':
      $(input).prop('checked', source_input.prop('checked'));
      break;
    case 'radio':
      $(input).prop('checked', source_input.prop('checked'));
      break;
    default:
      break;
  };

  //password, button, submit, file, reset, imageは除外。
  allow_input_type = ['text','hidden','checkbox','radio'];
  if( $.inArray($(input).attr('type'), allow_input_type) >= 0 ){
      $(target_form)[0].appendChild(input);
  };
};
function copyTextarea(target_form, source_textarea){
  var textarea = document.createElement('textarea');
  $(textarea).attr('name', source_textarea.attr('name')); 
  $(textarea).val(source_textarea.val());
  $(target_form)[0].appendChild(textarea);
};
function copySelect(target_form, source_select){
  var select = document.createElement('select');
  $(select).attr('name', source_select.attr('name')); 
  $(select).val(source_select.val());
  //複数選択可かどうかを判定して設定
  if ( $(select).attr('multiple')=='multiple' ){
    $(select).attr('multiple', 'multiple');
  }
  //option要素をコピー
  source_select.children('option').each(function(){
    var option = document.createElement('option');
    $(option).val($(this).val());
    $(option).prop('selected', $(this).prop('selected')); //選択状態をセット
    $(select)[0].appendChild(option);
  });
  $(target_form)[0].appendChild(select);
};

// 設定ファイルのダウンロードダイアログを表示
function showConfigDownloadDialog(){

  if( $("#item_configs > div").size() == 0 ){
    alert("アイテムを一つ以上追加してください。");
    return;
  };

  $("#save_config_dialog").dialog('open');
};

// 設定ファイルのアップロードダイアログを表示
function showConfigUploadDialog(){
  $("#load_config_dialog").dialog('open');
};

// 設定ファイル送信ボタンを押下時の確認ダイアログを表示
function confirmUploadConfig(){

  if( $("#item_configs > div").size() > 0 ){
    var result = confirm("設定ファイルを読み込むと、現在の状態は破棄されます。\nよろしいですか？");
    return result;
  };

};

function openTestResult(data){
  $("#test_data_dialog").dialog('open');
  $("#data_table").html(data);
  $("#test_data_dialog").height($("#data_table").height()+30);
};
/*
// テスト結果を別ウィンドウで表示させようとした名残。
function openTestResult(data){
  $('#data_table').html(data);

  var test_window;
  if(!test_window || test_window.closed ){
    var test_window = window.open("/generate_data/test", "IkasamaTest",
      "width=500, height=300, menubar=no, toolbar=no, resizable=yes, scrollbars=yes"
    );
    test_window.blur(); 
    test_window.focus(); 
  }else{
    test_window.blur(); 
    test_window.focus();
  }
};
*/

$(function($){

  //テスト時のAjaxの結果を処理
  $("#form_config")
  .bind("ajax:beforeSend", function(e, data, status, xhr){
    if(!validateForm()){return false;};
    showLoading();
  })
  .bind("ajax:success", function(e, data, status, xhr){
    openTestResult(data);
    //$('#data_table').html(data);
  })
  .bind("ajax:complete", function(e, data, status, xhr){
    hideLoading();
  });

  //設定ファイル読み込み時のAjaxの結果を処理
  $("#form_config_upload")
  .bind("ajax:success", function(e, data, status, xhr){

    $("#item_configs").empty();

    //Itemフォームを追加して設定ファイルの値で上書き。
    addItemByConfig(data.conf);

    //全体設定の値をフォームに設定
    updateGeneralConfigValues(data.genconf);

  });

  //保存ダイアログを初期化
  $("#save_config_dialog").dialog({
    autoOpen: false,
    modal: true,
    width: 400 
  });

  //読込ダイアログを初期化
  $("#load_config_dialog").dialog({
    autoOpen: false,
    modal: true,
    width: 480 
  });

  //テスト結果表示ダイアログを初期化
  $("#test_data_dialog").dialog({
    autoOpen: false,
    modal: false,
    width: 500,
    height: 400
  });

});

function clone(obj) {
  var f = function(){};
  f.prototype = obj;
  return new f;
}
