jQuery.validator.addClassRules({
  field_name: {
    required: true,
    maxlength: 255
  },
  column_name: {
    required: true,
    maxlength: 255
  },
  item_prefix: {
    maxlength: 255
  },
  item_suffix: {
    maxlength: 255
  },
  blank_ratio: {
    required: true,
    min: 0,
    max: 100
  }
});

// 数値アイテム
jQuery.validator.addClassRules({
  init_number: {
    required: true,
    number: true
  },
  max_number: {
    required: true,
    number: true
  },
  step_number: {
    required: true,
    number: true
  },
  init_number_r: {
    required: true,
    number: true
  },
  max_number_r: {
    required: true,
    number: true
  },
  zerofill_max: {
    required: true,
    min: 0,
    max: 50
  }
});

// 年齢アイテム
jQuery.validator.addClassRules({
  age_min: {
    required: true,
    number: true
  },
  age_max: {
    required: true,
    number: true
  }
});

// 日付アイテム
jQuery.validator.addClassRules({
  datetime_delimitter: {
    maxlength: 255
  }
});

// ユーザリスト
jQuery.validator.addClassRules({
  user_list_text: {
    maxlength: 1000
  }
});

// 連結アイテム
jQuery.validator.addClassRules({
  concat_delimitter: {
    maxlength: 255
  }
});

// 出力設定
jQuery.validator.addClassRules({
  rownum: {
    required: true,
    min: 1,
    max: 50000
  },
  sql_table_name: {
    required: true,
    maxlength: 255
  }
});
