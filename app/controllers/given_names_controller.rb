# -*- coding: utf-8 -*-

class GivenNamesController < ApplicationController
  # GET /given_names
  # GET /given_names.xml
  def index
    @given_names = GivenName.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @given_names }
    end
  end

  # GET /given_names/1
  # GET /given_names/1.xml
  def show
    @given_name = GivenName.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @given_name }
    end
  end

  # GET /given_names/new
  # GET /given_names/new.xml
  def new
    @given_name = GivenName.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @given_name }
    end
  end

  # GET /given_names/1/edit
  def edit
    @given_name = GivenName.find(params[:id])
  end

  # POST /given_names
  # POST /given_names.xml
  def create
    @given_name = GivenName.new(params[:given_name])

    respond_to do |format|
      if @given_name.save
        flash[:notice] = 'GivenName was successfully created.'
        format.html { redirect_to(@given_name) }
        format.xml  { render :xml => @given_name, :status => :created, :location => @given_name }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @given_name.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /given_names/1
  # PUT /given_names/1.xml
  def update
    @given_name = GivenName.find(params[:id])

    respond_to do |format|
      if @given_name.update_attributes(params[:given_name])
        flash[:notice] = 'GivenName was successfully updated.'
        format.html { redirect_to(@given_name) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @given_name.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /given_names/1
  # DELETE /given_names/1.xml
  def destroy
    @given_name = GivenName.find(params[:id])
    @given_name.destroy

    respond_to do |format|
      format.html { redirect_to(given_names_url) }
      format.xml  { head :ok }
    end
  end
end
