# -*- coding: utf-8 -*-

class GenerateDataController < ApplicationController

  def index
  end

  def generate
    # パラメータから設定情報をパース
    my_parser = parse(params)
    conf_list   = my_parser.conf_list
    @rows = Array.new

    # テスト出力
    # my_rows MyLib::MyRows
    @my_rows = conf_list.get_rows(10)
    case @my_rows.configures[:data_format]
      when 'table'
        @field_names = conf_list.configures[:table][:header_st]['field_name'] ? @my_rows.field_names : nil
        @column_names = conf_list.configures[:table][:header_st]['column_name'] ? @my_rows.column_names : nil
        @rows = @my_rows.rows
        render :partial => 'data_table'
#          render :template => 'generate_data/generate_table.js.erb'
        return
      when 'csv'
        render :text => '<div id="test_result">' + @my_rows.to_csv + '</div>'
        return
      when 'sql'
        render :text => '<div id="test_result">' + @my_rows.to_sql + '</div>'
        return
    end

  rescue MyLib::MyParamErrorException => ex
    logger.fatal("\n#{ex.class.name}: #{ex.message}")
    logger.fatal(ex.backtrace.join("\n"))
    logger.fatal("\n")
    @user_message = ex.user_message
    render :partial => 'error_comment'
    return
  rescue => ex
    logger.fatal("\n#{ex.class.name}: #{ex.message}")
    logger.fatal(ex.backtrace.join("\n"))
    logger.fatal("\n")
    render :partial => 'error_comment'
    return
  end

  def download
    # パラメータから設定情報をパース
    my_parser = parse(params)
    conf_list   = my_parser.conf_list
    @rows = Array.new

    # ダウンロード
    @my_rows = conf_list.get_rows_all
    data = ''
    case @my_rows.configures[:data_format]
      when 'table'
        @field_names = conf_list.configures[:table][:header_st]['field_name'] ? @my_rows.field_names : nil
        @column_names = conf_list.configures[:table][:header_st]['column_name'] ? @my_rows.column_names : nil
        @rows = @my_rows.rows
        data = render_to_string( :partial => 'data_table' )
      when 'csv'
        data = @my_rows.to_csv
      when 'sql'
        data = @my_rows.to_sql
    end

    data = '   ' + data
    data.setbyte(0, 0xEF)
    data.setbyte(1, 0xBB)
    data.setbyte(2, 0xBF)

    send_data data, :type => @my_rows.get_mime, :filename => 'data.'+@my_rows.get_extension
    return

  rescue MyLib::MyParamErrorException => ex
    logger.fatal("\n#{ex.class.name}: #{ex.message}")
    logger.fatal(ex.backtrace.join("\n"))
    logger.fatal("\n")
    @user_message = ex.user_message
    send_data @user_message, :type => 'text/plain', :filename => 'error.txt'
    return
  rescue => ex
    logger.fatal("\n#{ex.class.name}: #{ex.message}")
    logger.fatal(ex.backtrace.join("\n"))
    logger.fatal("\n")
    send_data 'エラーが発生しました。', :type => 'text/plain', :filename => 'error.txt'
    return
  end

  def config_download

    new_params = Hash.new
    if params['download_filename'].blank?
      file_name = 'ikasama_config.yml'
    else
      file_name = params['download_filename']
    end
    params.delete('download_filename')
    params.delete('controller')
    params.delete('action')
    params.to_hash.each{|key, value|
      new_params[key] = params_to_hash(value)
    }

    # paramsの内容を調べ、足りないパラメータがあった場合はnull値をセットする。
    params_conf_new = Hash.new
    params_conf_new['genconf'] = params_set_null(new_params['genconf'], DATAGEN_NULLDATA['genconf'])

    params_conf_new['conf'] = Hash.new
    new_params['conf'].each{|item_id, item_config|
      item_no_param = DATAGEN_NULLDATA['conf']['common'].merge(DATAGEN_NULLDATA['conf'][item_config['item_type']])
      params_conf_new['conf'][item_id] = params_set_null(item_config, item_no_param) 
    }

    #config_tmp = JSON.pretty_generate(new_params)
    config_tmp = params_conf_new.to_yaml

    send_data config_tmp, :type => 'application/yaml', :filename => file_name

  end

  def config_upload

    file = params[:file][:config]
    config_yaml = YAML.load(file.read)
    config_json = config_yaml.to_json
    render :json => config_json, :status => 200
    return

  end

private
  def parse(conf_param)

    parser = MyLib::MyConfParser.new(conf_param)
    return parser

  end

  def params_to_hash(params)
    if params.class.name == 'ActiveSupport::HashWithIndifferentAccess'
      new_params = Hash.new
      params.to_hash.each{|key, val|
        new_params[key] = params_to_hash(val)
      }
      return new_params
    else
      return params
    end
  end

  #設定をダウンロードする時はチェックボックスの値が外れていると
  #パラメータそのものが送られてこないのでnullを明示的に設定する。
  def params_set_null(params, params_default)
    if params.class.name == 'Hash'
      params_default.each{|key, val|
        if params.has_key?(key)
          params[key] = params_set_null(params[key], params_default[key])
        else
          params[key] = val 
        end 
      }
    else
      return params
    end
    return params
  end

  def params_set_default(params, params_default)
    if params.class.name == 'Hash'
      params_default.each{|key, val|
        if params.has_key?(key)
#          params_set_default(, params_default[key])
        else
          params[key] = val
        end 
      }
    else
      return params
    end
  end

end
