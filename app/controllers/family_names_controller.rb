# -*- coding: utf-8 -*-

class FamilyNamesController < ApplicationController
  # GET /family_names
  # GET /family_names.xml
  def index
    @family_names = FamilyName.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @family_names }
    end
  end

  # GET /family_names/1
  # GET /family_names/1.xml
  def show
    @family_name = FamilyName.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @family_name }
    end
  end

  # GET /family_names/new
  # GET /family_names/new.xml
  def new
    @family_name = FamilyName.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @family_name }
    end
  end

  # GET /family_names/1/edit
  def edit
    @family_name = FamilyName.find(params[:id])
  end

  # POST /family_names
  # POST /family_names.xml
  def create
    @family_name = FamilyName.new(params[:family_name])

    respond_to do |format|
      if @family_name.save
        flash[:notice] = 'FamilyName was successfully created.'
        format.html { redirect_to(@family_name) }
        format.xml  { render :xml => @family_name, :status => :created, :location => @family_name }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @family_name.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /family_names/1
  # PUT /family_names/1.xml
  def update
    @family_name = FamilyName.find(params[:id])

    respond_to do |format|
      if @family_name.update_attributes(params[:family_name])
        flash[:notice] = 'FamilyName was successfully updated.'
        format.html { redirect_to(@family_name) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @family_name.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /family_names/1
  # DELETE /family_names/1.xml
  def destroy
    @family_name = FamilyName.find(params[:id])
    @family_name.destroy

    respond_to do |format|
      format.html { redirect_to(family_names_url) }
      format.xml  { head :ok }
    end
  end
end
